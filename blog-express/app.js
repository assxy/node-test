var createError = require('http-errors');//处理404
var express = require('express');
var path = require('path');
var fs = require('fs')
var cookieParser = require('cookie-parser'); //解析 cookie
var logger = require('morgan'); //日志
const session = require('express-session'); // npm i express-session --save
// const bodyParser = require('body-parser'); // npm install body-parser --save
const RedisStore = require('connect-redis')(session) //npm i connect-redis --save

// var indexRouter = require('./routes/index');
// var usersRouter = require('./routes/users');
var blogRouter = require('./routes/blog');
var userRouter = require('./routes/user');

var app = express(); // 初始化app，本次http请求的实例

// // view engine setup 视图引擎的设置 （忽略）
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'jade');

const ENV = process.env.NODE_ENV
if (ENV!=='production') {
  //开发环境 / 测试环境
  app.use(logger('dev',{
    stream:process.stdout //默认参数
  })); //日志
}else{
  //线上环境
  const logFileName = path.join(__dirname,'logs','access.log')
  const writeStream = fs.createWriteStream(logFileName,{
    flags:'a'
  })
  app.use(logger('combined',{
    stream:writeStream
  })); //日志
}


// app.use(bodyParser.json())
// app.use(bodyParser.urlencoded({ extended: false }))
app.use(express.json()); // 获取的是Content-type等于application/json格式的数据 同 处理post data 经过处理后可以在路由中直接用req.body获取前台传过来的值
app.use(express.urlencoded({ extended: false }));//同上 为了解析urlencoded格式的数据
app.use(cookieParser()); //解析 cookie 经过处理后可以在路由中直接req.cookie访问cookie里的内容
// app.use(express.static(path.join(__dirname, 'public')));//对应public静态文件夹 （忽略）

const redisClient = require('./db/redis')
const sessionStore = new RedisStore({
  client:redisClient
})
app.use(session({
  secret: 'Wzq54@32._', //自定义一个密匙
  cookie: {
    // path:'/', //默认配置
    // hpptOnly:true, //默认配置
    maxAge: 24 * 60 * 60 * 1000 //24小时失效
  },
  store:sessionStore
}))


// app.use('/', indexRouter); //注册路由
// app.use('/users', usersRouter);//同上
app.use('/api/blog', blogRouter);
app.use('/api/user', userRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {//处理404
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'dev' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

const Koa = require('koa')
const app = new Koa() // 当前请求实例
const views = require('koa-views') //对应views文件夹
const json = require('koa-json') //postData 格式处理
const onerror = require('koa-onerror')
const bodyparser = require('koa-bodyparser')//postData数据存到body里
const logger = require('koa-logger')//日志
const session = require('koa-generic-session') // npm i koa-generic-session koa-redis redis --save
const redisStore = require('koa-redis') //同上
const path = require('path')
const fs = require('fs')
const morgan = require('koa-morgan')//npm i koa-morgan --save


const index = require('./routes/index') //路由
const users = require('./routes/users') //路由
const blog = require('./routes/blog') //路由
const user = require('./routes/user') //路由


const {REDIS_CONF}=require('./conf/db')

// error handler 监测
onerror(app)

// middlewares 引用中间件
app.use(bodyparser({                      //处理post请求格式最终处理成json格式
  enableTypes:['json', 'form', 'text']    //同上
}))                                       //同上
app.use(json())                           //同上
app.use(logger())
app.use(require('koa-static')(__dirname + '/public'))

app.use(views(__dirname + '/views', {
  extension: 'pug'
}))

// logger  当前请求耗时
app.use(async (ctx, next) => {
  const start = new Date()
  await next()
  const ms = new Date() - start
  console.log(`${ctx.method} ${ctx.url} - ${ms}ms`)
})

const ENV = process.env.NODE_ENV
if (ENV!=='production') {
  //开发环境 / 测试环境
  app.use(morgan('dev',{
    stream:process.stdout //默认参数
  })); //日志
}else{
  //线上环境
  const logFileName = path.join(__dirname,'logs','access.log')
  const writeStream = fs.createWriteStream(logFileName,{
    flags:'a'
  })
  app.use(morgan('combined',{
    stream:writeStream
  })); //日志
}



//session配置
app.keys=['Wzq54@32._'] //自定义一个密匙
app.use(session({
  //配置cookie
  cookie:{
    path:'/',
    httpOnly:true,
    maxAge:24*60*60*1000
  },
  //配置redis
  store:redisStore({
    // all:'127.0.0.1.6379' //先写死本地的redis
    all:`${REDIS_CONF.host}:${REDIS_CONF.port}`
  })
}))


// routes
app.use(index.routes(), index.allowedMethods())
app.use(users.routes(), users.allowedMethods())
app.use(blog.routes(), users.allowedMethods())
app.use(user.routes(), users.allowedMethods())

// error-handling
app.on('error', (err, ctx) => {
  console.error('server error', err, ctx)
});

module.exports = app

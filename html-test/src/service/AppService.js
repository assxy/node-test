import {post,get} from './http.js'


export default {
  //登录
  // login({username,password}) {
  //   return new Promise((resolve, reject) =>{
  //     post("/api/user/login",{username,password})
  //     .then(response => {
  //       resolve(response);
  //     }, err => {
  //       reject(err)
  //     })
  //   })
  // },
  login(data) {
    return post("/api/user/login",data);
  },
  //删除博客
  del(data) {
    return post("/api/blog/del",data);
  },
  //更新博客
  update(data) {
    return post("/api/blog/update",data);
  },
  //新建博客
  newblog(data) {
    return post("/api/blog/new",data);
  },
  //博客详情
  detail(data) {
    return get("/api/blog/detail",data);
  },
  //博客列表
  list(data) {
    return get("/api/blog/list",data);
  }
}

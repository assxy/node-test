const fs = require('fs')
const path = require('path')



//写日志
function writeLog(writeStream,log){
    writeStream.write(log+'\n')
}

//生成 write Stream
function createWriteStream(fileName){
    const fullFileName = path.join(__dirname,'../','../','logs','access.log')
    const writeStream = fs.createWriteStream(fullFileName,{
        flags:'a'
    })
    return writeStream
}

//写访问日志
const accessWtiteStream = createWriteStream('access.log')
function access(log){
    writeLog(accessWtiteStream,log)
}

module.exports={
    access
}
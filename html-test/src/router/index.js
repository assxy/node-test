import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
const index = () => import('@/pages/index')
const admin = () => import('@/pages/admin')
const edit = () => import('@/pages/edit')
const detail = () => import('@/pages/detail')
const login = () => import('@/pages/login')
const newpage = () => import('@/pages/new')

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/helloword',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/',
      name: 'index',
      component: index
    },
    {
      path: '/admin',
      name: 'admin',
      component: admin
    },
    {
      path: '/edit',
      name: 'edit',
      component: edit
    },
    {
      path: '/detail',
      name: 'detail',
      component: detail
    },
    {
      path: '/login',
      name: 'login',
      component: login
    },
    {
      path: '/newpage',
      name: 'newpage',
      component: newpage
    }
  ]
})

const fs = require('fs')
const path = require('path')

// // callback 方式获取一个文件的内容
// function getFileContent(fileName,callback){
//     const fullFileName = path.resolve(__dirname,'files',fileName)
//     fs.readFile(fullFileName,(err,data)=>{
//         if (err) {
//             console.error(err)
//             return
//         }
//         callback(
//             JSON.parse(data.toString())
//         )
//     })
// }

// //测试 callback-hell(回调地狱)
// getFileContent('a.json',aData=>{
//     console.log('a data',aData)
//     getFileContent(aData.next,bData=>{
//         console.log('b data',bData)
//         getFileContent(bData,cData=>{
//             console.log('c data',cData)
//         })
//     })
// })

//用 promise 获取文件内容
function getFileContent(fileName) {
    const promise = new Promise((resolve, reject) => {
        const fullFileName = path.resolve(__dirname, 'files', fileName)
        fs.readFile(fullFileName, (err, data) => {
            if (err) {
                reject(err)
                return
            }
            resolve(
                JSON.parse(data.toString())
            )
        })
    })
    return promise
}

// getFileContent('a.json').then(aData => {
//     console.log('a data', aData)
//     return getFileContent(aData.next)
// }).then(bData => {
//     console.log('b data', bData)
//     return getFileContent(bData.next)
// }).then(cData => {
//     console.log('c data', cData)
// })

async function readFileData(){
    // 同步写法
    try {
        const aData = await getFileContent('a.json')
        console.log('a data', aData)
        const bData = await getFileContent(aData.next)
        console.log('b data', bData)
        const cData = await getFileContent(bData.next)
        console.log('c data', cData)
    } catch (error) {
        console.error(error)
    }
}

readFileData()

// async function readAData(){
//     const aData = await getFileContent('a.json')
//     return aData
// }
// async function test(){
//     const aData = await readAData()
//     console.log('a data', aData)
// }
// test()

// async await (异步函数)要点：
// 1.await后面可以追加promise对象，获取resolve的值
// 2.await必须包裹咋async函数里面
// 3.async函数执行返回的也是一个promise对象
// 4.try-catch 截获 promise 中的reject的值
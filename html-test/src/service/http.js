import axios from 'axios';
// import qs from 'qs';
import router from '../router';


//设置axios请求参数。
axios.defaults.timeout = 5000; //请求超时5秒
axios.defaults.baseURL = ''; //请求base url
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'; //设置post请求是的header信息
axios.defaults.headers.get['Content-Type'] = 'application/json'; //设置post请求是的header信息

//如果你要用到session验证码功能，让请求携带cookie，可以加上以下一行：
axios.defaults.withCredentials = true

//添加请求拦截器
axios.interceptors.request.use(
  config => {
    // 在发送请求之前做些什么
    return config
  },
  err => {
    // 对请求错误做些什么
    return Promise.reject(err)
  }
)

//添加响应拦截器
axios.interceptors.response.use(
  response => {
    // 对响应数据做点什么
    if (response.data.error==-1) { //error=0请求成功,error=-1请求错误
      alert(response.data.message);
      return
    }
    return response
  },
  err => {
    // 对响应错误做点什么

    return Promise.reject(err)
  }
)

/**
 * 封装get方法
 * @param url
 * @param data
 * @returns {Promise}
 */

export function get(url, params={}){
  return new Promise((resolve, reject) => {
    axios.get(url,{
      params: params
    })
    .then(response => {
      resolve(response.data);
    })
    .catch(err => {
      reject(err)
    })
  })
}

/**
 * 封装post请求
 * @param url
 * @param data
 * @returns {Promise}
 */

 export function post(url, data = {}){
   return new Promise((resolve, reject) => {
      axios.post(url, data)
      .then(response => {
        resolve(response.data);
      }, err => {
        reject(err)
      })
   })
 }
